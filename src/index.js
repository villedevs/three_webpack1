import * as THREE from 'three';
import * as OrbitControls from 'three-orbitcontrols';


import {MyScene} from './scene';

const myScene = new MyScene();
myScene.setup(window.innerWidth, window.innerHeight);

// create a renderer & add to DOM
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const controls = new OrbitControls(myScene.getCamera(), renderer.domElement);


// set & start rendering the scene
const render = () => {
  requestAnimationFrame(render);
  myScene.render(renderer);
};
render();

window.addEventListener('resize', () => {
  myScene.resize(window.innerWidth, window.innerHeight);
  renderer.setSize(window.innerWidth, window.innerHeight);
}, false);