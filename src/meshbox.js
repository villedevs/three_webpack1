import * as THREE from 'three';

export const createMeshBox = (params) => {
  const texture = new THREE.TextureLoader().load(params.albedoURL);

  const geometry = new THREE.BoxGeometry(params.width, params.height, params.depth);
  const material = new THREE.MeshPhongMaterial({color: 0xffffff, shininess: 50, map: texture});
  const mesh = new THREE.Mesh(geometry, material);
  return mesh;
};