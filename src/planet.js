import * as THREE from 'three';

export const createPlanet = (params) => {
  const texture = new THREE.TextureLoader().load(params.albedoURL);

  const geometry = new THREE.SphereGeometry(params.radius, 32, 32);
  const material = new THREE.MeshPhongMaterial({color: 0xffffff, shininess: 50, map: texture});
  const planet = new THREE.Mesh(geometry, material);
  return planet;
};