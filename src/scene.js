import * as THREE from 'three';
import GLTFLoader from 'three-gltf-loader';

import {createPlanet} from './planet';
import {createMeshBox} from './meshbox';

export class MyScene {

  constructor() {
    this.angle = 0.0;
  }

  setup(windowWidth, windowHeight) {
    // create a scene
    this.scene = new THREE.Scene();
    
    const envTex = new THREE.TextureLoader().load("http://vl-three.s3-website.eu-central-1.amazonaws.com/V1110154.JPG");

    const envGeo = new THREE.SphereGeometry(500, 60, 40);
    const envMat = new THREE.MeshBasicMaterial({map: envTex, side: THREE.DoubleSide});
    this.envBox = new THREE.Mesh(envGeo, envMat);
    this.scene.add(this.envBox);



    this.mars = createPlanet({
      albedoURL: "http://vl-three.s3-website.eu-central-1.amazonaws.com/2k_mars.jpg",
      radius: 10,
    });
    this.scene.add(this.mars);

    this.earth = createPlanet({
      albedoURL: "http://vl-three.s3-website.eu-central-1.amazonaws.com/earth_atmos_2048.jpg",
      radius: 15,
    });
    this.scene.add(this.earth);

    this.cube = createMeshBox({
      albedoURL: "http://vl-three.s3-website.eu-central-1.amazonaws.com/elon.jpg",
      width: 10,
      height: 10,
      depth: 10,
    });
    this.scene.add(this.cube);

    // add some light
    const light = new THREE.PointLight(0xffabba);
    light.position.set(10, 10, 35);
    light.intensity = 2;
    this.scene.add(light);

    const ambLight = new THREE.AmbientLight({color: 0x101010});
    ambLight.intensity = 0.5;
    this.scene.add(ambLight);

    // create a camera and set position
    this.camera = new THREE.PerspectiveCamera(75, (windowWidth / windowHeight), 0.1, 1000);
    this.camera.position.z = 80;

    const loader = new GLTFLoader();
    loader.load('http://vl-three.s3-website.eu-central-1.amazonaws.com/Duck.gltf',
    (gltf) => {
        this.duck = gltf.scene;
        this.duck.scale.set(30, 30, 30);
        this.scene.add(this.duck);
    },
    (xhr) => {
        console.log(`${( xhr.loaded / xhr.total * 100 )}% loaded`);
    },
    (error) => {
        console.error('An error happened', error);
    }
    );
  }

  getCamera() {
    return this.camera;
  }

  resize(width, height) {
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
  }

  render(renderer) {
    //this.box.rotation.x += 0.02;
    //this.box.rotation.y += 0.03;
    //this.box.rotation.z += 0.05;
  
    this.mars.position.x = Math.sin(this.angle * Math.PI / 180.0) * 50.0;
    this.mars.position.z = Math.cos(this.angle * Math.PI / 180.0) * 50.0;

    this.earth.position.x = Math.sin((this.angle - 45.0) * Math.PI / 180.0) * 50.0;
    this.earth.position.z = Math.cos((this.angle - 45.0) * Math.PI / 180.0) * 50.0;
  
    this.cube.position.x = Math.sin((this.angle + 100.0) * Math.PI / 180.0) * 50.0;
    this.cube.position.z = Math.cos((this.angle + 100.0) * Math.PI / 180.0) * 50.0;
    this.cube.rotation.x += 0.01;
    this.cube.rotation.y -= 0.01;
    this.cube.rotation.z += 0.02;

    this.duck.rotation.x += 0.05;
    this.duck.rotation.y += 0.02;
    this.duck.rotation.z -= 0.04;
  
    this.angle += 1.1;

    renderer.render(this.scene, this.camera);
  }
}